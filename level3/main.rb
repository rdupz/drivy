require "json"
require "date"
require "pp"



# Compute the price according to the decreasing pricing rules
def price_days(nb_days, price_per_day)

  price_days = 0

  if nb_days > 10
    price_days += (nb_days - 10) * (price_per_day * 0.5)
    nb_days = 10
  end

  if nb_days > 4
    price_days += (nb_days - 4) * (price_per_day * 0.7)
    nb_days = 4
  end

  if nb_days > 1
    price_days += (nb_days - 1) * (price_per_day * 0.9)
    nb_days = 1
  end

  price_days +=  nb_days * price_per_day

end



# Compute the commission and split it between actors.
def commission(nb_days, rental_price)

  commission = rental_price * 0.3

  insurance_fee  = commission / 2
  assistance_fee = nb_days * 100
  drivy_fee      = commission - (insurance_fee + assistance_fee)

   {:insurance_fee  => insurance_fee.to_i,
    :assistance_fee => assistance_fee.to_i,
    :drivy_fee      => drivy_fee.to_i }
end



data = JSON.parse( IO.read("data.json"), object_class: OpenStruct )
cars    = data.cars
rentals = data.rentals


rentals_prices = []


rentals.each { |rental|

  car = cars.find {|c| c.id == rental.car_id }

  price_kms  = rental.distance * car.price_per_km
  days       = (Date.parse(rental.end_date) - Date.parse(rental.start_date)).to_i + 1
  price_days = price_days(days, car.price_per_day)
  price      = price_days + price_kms
  commission = commission(days, price)

  rentals_prices <<  {:id => rental.id,
                      :price => price,
                      :commission => commission }
}


output = {:rentals => rentals_prices}
pp output