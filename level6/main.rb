require "json"
require "date"
require "pp"



DEDUCTIBLE_REDUCTION_PER_DAY = 400
ASSISTANCE_FEE_PER_DAY       = 100
COMMISSION_PERCENT           = 30
DISCOUNTS = {
    1  => 0.9,
    4  => 0.7,
    10 => 0.5}



# An Action represents a bank operation.
# It defines who will lose/gain a amount of money.
class Action
  attr_accessor :who
  attr_accessor :type
  attr_accessor :amount

  def initialize(who, type, amount)
    @who = who
    @type = type
    @amount = amount
  end

  def hashify_the_drivy_way
    {:who => @who, :type => @type, :amount => @amount}
  end
end



# A Rental is a car booking.
class Rental
  attr_accessor :car
  attr_accessor :start_date
  attr_accessor :end_date
  attr_accessor :distance
  attr_accessor :deductible_reduction


  def initialize(rental, car)
    @start_date = rental.start_date
    @end_date   = rental.end_date
    @distance   = rental.distance
    @deductible_reduction = rental.deductible_reduction
    @car = car
  end


  # Change this object based on a rental_modification request.
  def modify(rental_modification)

    # Alter a copy to avoid spaghetti code.
    # Performance is not a requirement, but clean code is.
    copy = self.dup
    if rental_modification.start_date
      copy.start_date = rental_modification.start_date
    end
    if rental_modification.end_date
      copy.end_date = rental_modification.end_date
    end
    if rental_modification.distance
      copy.distance = rental_modification.distance
    end

    # Check validy of the copy
    if Date.parse(copy.start_date) > Date.parse(copy.end_date)
      raise ArgumentError.new('Start date must be before end date.')
    end
    if copy.distance <= 0
      raise ArgumentError.new('Distance must be positive.')
    end

    # If copy is ok then modify self
    @start_date = copy.start_date
    @end_date   = copy.end_date
    @distance   = copy.distance
  end


  def price_kms
    @distance * @car.price_per_km
  end


  def days
    (Date.parse(@end_date) - Date.parse(@start_date)).to_i + 1
  end


  def price_days
    price_days = 0
    nb_days = days

    DISCOUNTS.sort.reverse.to_h.each do |required_day, discount|
      if nb_days > required_day
        price_days += (nb_days - required_day) * (@car.price_per_day * discount)
        nb_days = required_day
      end
    end

    price_days +=  nb_days * @car.price_per_day
  end


  def price
    price_kms + price_days
  end


  def deductible_reduction_amount
    self.deductible_reduction ? days * DEDUCTIBLE_REDUCTION_PER_DAY : 0
  end


  def commission
    commission = price * (COMMISSION_PERCENT/100.0)

    insurance_fee  = commission / 2
    assistance_fee = days * ASSISTANCE_FEE_PER_DAY
    drivy_fee      = commission - (insurance_fee + assistance_fee)

    {:insurance_fee  => insurance_fee,
     :assistance_fee => assistance_fee,
     :drivy_fee      => drivy_fee }
  end


  def actions
    commission_fees = commission
    actions = []
    actions << Action.new("driver",     "debit",  price + deductible_reduction_amount)
    actions << Action.new("owner",      "credit", price * (100-COMMISSION_PERCENT)/100.0)
    actions << Action.new("insurance",  "credit", commission_fees[:insurance_fee])
    actions << Action.new("assistance", "credit", commission_fees[:assistance_fee])
    actions << Action.new("drivy",      "credit", commission_fees[:drivy_fee] + deductible_reduction_amount)
  end
end



# Helper to switch between "credit" and "debit"
def switch_type_helper(type)
  if type == "credit"
    "debit"
  else
    "credit"
  end
end



#---------------------
#    Main script
#---------------------

data = JSON.parse( IO.read("data.json"), object_class: OpenStruct )
cars                 = data.cars
rentals              = data.rentals
rental_modifications = data.rental_modifications


rentals_delta = []


rental_modifications.each do |rental_modification|

  rental_json = rentals.find {|r| r.id == rental_modification.rental_id}
  car    = cars.find {|c| c.id == rental_json.car_id }

  rental = Rental.new(rental_json, car)
  rental_modified = rental.clone
  rental_modified.modify(rental_modification)

  # Compute the delta amounts between rental and rental_modified.
  # Store new 'delta' actions into actions.
  actions = []
  rental_modified_actions = rental_modified.actions
  rental.actions.each_with_index do |action, i|
    delta   = rental_modified_actions[i].amount - action.amount
    next if delta == 0
    type    =  delta > 0 ? action.type : switch_type_helper(action.type)
    actions << Action.new(action.who, type, delta.abs)
  end

  rentals_delta <<  { :id       => rental_modification.id,
                     :rental_id => rental_json.id,
                     :actions   => actions.map {|a| a.hashify_the_drivy_way}}
end


output = {:rental_modifications => rentals_delta}
pp output