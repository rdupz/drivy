require "json"
require "date"
require "pp"



data = JSON.parse( IO.read("data.json"), object_class: OpenStruct )
cars    = data.cars
rentals = data.rentals


rentals_prices = []


rentals.each { |rental|

  car = cars.find {|c| c.id == rental.car_id }

  price_kms  = rental.distance * car.price_per_km
  days       = (Date.parse(rental.end_date) - Date.parse(rental.start_date)).to_i + 1
  price_days = days * car.price_per_day
  price      = price_days + price_kms

  rentals_prices <<  {:id => rental.id, :price => price }
}


output = {:rentals => rentals_prices}
pp output