require "json"
require "date"
require "pp"



# Compute the price according to the decreasing pricing rules
def price_days(nb_days, price_per_day)

  price_days = 0

  if nb_days > 10
    price_days += (nb_days - 10) * (price_per_day * 0.5)
    nb_days = 10
  end

  if nb_days > 4
    price_days += (nb_days - 4) * (price_per_day * 0.7)
    nb_days = 4
  end

  if nb_days > 1
    price_days += (nb_days - 1) * (price_per_day * 0.9)
    nb_days = 1
  end

  price_days +=  nb_days * price_per_day

end



data = JSON.parse( IO.read("data.json"), object_class: OpenStruct )
cars    = data.cars
rentals = data.rentals


rentals_prices = []


rentals.each { |rental|

  car = cars.find {|c| c.id == rental.car_id }

  price_kms  = rental.distance * car.price_per_km
  days       = (Date.parse(rental.end_date) - Date.parse(rental.start_date)).to_i + 1
  price_days = price_days(days, car.price_per_day)
  price      = price_days + price_kms

  rentals_prices <<  {:id => rental.id, :price => price }
}


output = {:rentals => rentals_prices}
pp output